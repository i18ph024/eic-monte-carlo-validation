BEGIN PLOT /H1_2004_I657925/d05-x01-y01
Title=Averaged pT cross section for single $D^+$ production
XLabel=$pT (GeV)$
YLabel=$d\sigma/dpT (nb/GeV)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

# ... add more histograms as you need them ...

BEGIN PLOT /H1_2004_I657925/d06-x01-y01
Title=Averaged $\eta$ cross section for single $D^+$ production
XLabel=$\eta$
YLabel=$d\sigma/d\eta (nb)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d07-x01-y01
Title=Averaged $Q^2$ cross section for single $D^+$ production
XLabel=$Q^2 (GeV^2)$
YLabel=$d\sigma/dQ^2 (nb/GeV^2)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d08-x01-y01
Title=Averaged pT cross section for single $D^0$ production
XLabel=$pT (GeV)$
YLabel=$d\sigma/dpT (nb/GeV)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d09-x01-y01
Title=Averaged $\eta$ cross section for single $D^0$ production
XLabel=$\eta$
YLabel=$d\sigma/d\eta (nb)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d10-x01-y01
Title=Averaged $Q^2$ cross section for single $D^0$ production
XLabel=$Q^2 (GeV^2)$
YLabel=$d\sigma/dQ^2 (nb/GeV^2)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d11-x01-y01
Title=Averaged pT cross section for single $D_S^+$ production
XLabel=$pT (GeV)$
YLabel=$d\sigma/dpT (nb/GeV)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d12-x01-y01
Title=Averaged $\eta$ cross section for single $D_S^+$ production
XLabel=$\eta$
YLabel=$d\sigma/d\eta (nb)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d13-x01-y01
Title=Averaged $Q^2$ cross section for single $D_S^+$ production
XLabel=$Q^2 (GeV^2)$
YLabel=$d\sigma/dQ^2 (nb/GeV^2)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d14-x01-y01
Title=Averaged pT cross section for single $D*^+$ production
XLabel=$pT (GeV)$
YLabel=$d\sigma/dpT (nb/GeV)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d15-x01-y01
Title=Averaged $\eta$ cross section for single $D*^+$ production
XLabel=$\eta$
YLabel=$d\sigma/d\eta (nb)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2004_I657925/d16-x01-y01
Title=Averaged $Q^2$ cross section for single $D*^+$ production
XLabel=$Q^2 (GeV^2)$
YLabel=$d\sigma/dQ^2 (nb/GeV^2)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT
