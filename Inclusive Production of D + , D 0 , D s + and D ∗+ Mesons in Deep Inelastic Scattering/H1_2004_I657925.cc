// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DISFinalState.hh"


namespace Rivet {


  /// @brief Add a short analysis description here
  class H1_2004_I657925 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(H1_2004_I657925);


    /// @name Analysis methods
    ///@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
        FinalState fs;
        const DISKinematics& diskin = DISKinematics();
        declare(diskin,"Kinematics");
        declare(UnstableParticles(), "UPS");


      // Book histograms
      // specify custom binning
      // take binning from reference data using HEPData ID (digits in "d01-x01-y01" etc.)
 /*   book(_h["111"], 1, 1, 1); // Broken Histogram
      book(_h["211"], 2, 1, 1); // Broken Histogram
      book(_h["311"], 3, 1, 1); // Broken Histogram
      book(_h["411"], 4, 1, 1); */
      book(_h["511"], 5, 1, 1); 
      book(_h["611"], 6, 1, 1);
      book(_h["711"], 7, 1, 1);
      book(_h["811"], 8, 1, 1);
      book(_h["911"], 9, 1, 1);
      book(_h["1011"], 10, 1, 1);
      book(_h["1111"], 11, 1, 1);
      book(_h["1211"], 12, 1, 1);
      book(_h["1311"], 13, 1, 1);
      book(_h["1411"], 14, 1, 1);
      book(_h["1511"], 15, 1, 1);
      book(_h["1611"], 16, 1, 1);
      
      // counter pointer to store the no. of events           
      book(_Nevt_after_cuts, "TMP/Nevt_after_cuts");
      

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
    
    /// DIS kinematics
    const DISKinematics& dk = apply<DISKinematics>(event, "Kinematics");
    double q2  = dk.Q2();
    double x   = dk.x();
    double y   = dk.y();

     ///Assure the event falls into the kinematic range of the measurement.
     if (!inRange(q2/GeV2, 2, 100)) vetoEvent;
     if (!inRange(y, 0.05, 0.7)) vetoEvent;
     _Nevt_after_cuts -> fill();
     


       const UnstableParticles& ufs = apply<UnstableParticles>(event, "UPS");
          
 for (const Particle& p : filter_select(ufs.particles(), Cuts::abspid == PID::D0)) {
              ///But not not from \f$D^{*\pm}$\f decays
              if (p.hasAncestor(PID::DSTARPLUS)) continue;
              if (p.hasAncestor(PID::DSTARMINUS)) continue;
              /// Select particles only in the \f$\eta-p_{T}$\f region
              if (!inRange(p.eta(), -1.5, 1.5)) continue;
              if (p.pt()/GeV < 2.5) continue;
              /// Fill the general histograms
              _h["811"]->fill(p.pt()/GeV);
              _h["911"]->fill(p.eta());
              _h["1011"]->fill(q2/GeV2);

          }
          
 for (const Particle& p : filter_select(ufs.particles(), Cuts::abspid == PID::DPLUS)) {
              /// Select particles only in the \f$\eta-p_{T}$\f region
              if (!inRange(p.eta(), -1.5, 1.5)) continue;
              if (p.pt()/GeV < 2.5) continue;
              /// Fill the general histograms
              _h["511"]->fill(p.pt()/GeV);
              _h["611"]->fill(p.eta());
              _h["711"]->fill(q2/GeV2);
       
          }

 for (const Particle& p : filter_select(ufs.particles(), Cuts::abspid == 431)) { //DSPLUS
    
       if (!inRange(p.eta(), -1.5, 1.5)) continue;
       if (p.pt()/GeV < 2.5) continue;
       /// Fill the general histograms
       _h["1111"]->fill(p.pt()/GeV);
       _h["1211"]->fill(p.eta());
       _h["1311"]->fill(q2/GeV2);
    }
    
 for (const Particle& p : filter_select(ufs.particles(), Cuts::abspid == 413)) { //DSTARPLUS
       if (!inRange(p.eta(), -1.5, 1.5)) continue;
       if (p.pt()/GeV < 2.5) continue;
       /// Fill the general histograms
       _h["1411"]->fill(p.pt()/GeV);
       _h["1511"]->fill(p.eta());
       _h["1611"]->fill(q2/GeV2);
    }
}

    /// Normalise histograms etc., after the run
    void finalize() {
    
    const double sf = crossSection()/nanobarn/sumOfWeights();

      //normalize(_h["511"], crossSection()/picobarn); // normalize to generated cross-section in fb (no cuts)
      scale(_h["511"], sf);
      scale(_h["611"], sf);
      scale(_h["711"], sf);
      scale(_h["811"], sf);
      scale(_h["911"], sf);
      scale(_h["1011"], sf);
      scale(_h["1111"], sf);
      scale(_h["1211"], sf);
      scale(_h["1311"], sf);
      scale(_h["1411"], sf);
      scale(_h["1511"], sf);
      scale(_h["1611"], sf);

    }

    ///@}


    /// @name Histograms
    ///@{
    map<string, Histo1DPtr> _h;
    map<string, Profile1DPtr> _p;
    map<string, CounterPtr> _c;
    ///@}
    CounterPtr _Nevt_after_cuts;


  };


  DECLARE_RIVET_PLUGIN(H1_2004_I657925);

}
